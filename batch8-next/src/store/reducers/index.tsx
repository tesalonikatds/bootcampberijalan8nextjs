import dataCount from "./dataCount";
import { combineReducers } from "@reduxjs/toolkit";

const rootReducer = combineReducers({
  dataCount,
});
export default rootReducer;
