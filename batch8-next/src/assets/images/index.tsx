const IMAGES = {
  logoBerijalan: "/img/logo.png",
  logoBerijalanLight: "/img/logoLight.png",
};

export default IMAGES;
