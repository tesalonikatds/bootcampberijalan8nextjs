import Container from "@components/organisms/container";
import { cookies } from "next/headers";

export default function Home() {
  const auth = cookies().get("_userInfo");
  return (
    // <Container>
    <div className="min-h-screen">
      <h1 className="text-7xl">Hallo</h1>
      <h1>{auth?.value}</h1>
    </div>
    // </Container>
  );
}
