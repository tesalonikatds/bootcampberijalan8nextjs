"use server";

import satellite from "@services/satellite";
import { FormLogin } from "./fromLogin";
import { cookies } from "next/headers";

export default async function SubmitForm(data: FormLogin) {
  return await satellite
    .post("https://be-rmy.dev.berijalan.co.id/rest/v1/auth/login", data)
    .then((response) => {
      console.log("RESPONSE SUCCESS", response);
      cookies().set("_userInfo", JSON.stringify(response.data.data));
      return "Login Berhasil";
    })
    .catch((error) => {
      console.log("RESPONSE ERROR", error);
      throw new Error("Login Gagal");
    });
}
