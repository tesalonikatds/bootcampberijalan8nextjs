import Image from "next/image";
import Container from "@components/organisms/container";

export const metadata = {
  title: "Not Found",
};
export default function NotFound() {
  return (
    //<Container>
    <div className="min-h-screen">
      <h1 className="text-7xl">Halaman Tidak Di Temukan</h1>
    </div>
    //</Container>
  );
}
