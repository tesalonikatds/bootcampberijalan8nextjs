"use client";

import Header from "@components/molecules/Header";
import Sidebar from "@components/molecules/Sidebar";

import { Provider } from "react-redux";
import { store } from "@store/storage";
import React from "react";

export default function Container({ children }: { children: React.ReactNode }) {
  return (
    <Provider store={store}>
      <section>
        <Header />
        <div className="flex">
          <Sidebar />
          <div>{children}</div>
        </div>
      </section>
    </Provider>
  );
}
