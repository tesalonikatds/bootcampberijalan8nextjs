"use client";

import Logo from "@components/atoms/Logo";
import Theme from "@components/atoms/Theme";
import Menu from "@components/atoms/Menu";
import SidebarIcon from "@assets/ic_sidebar";
import { getItem, setItem } from "@store/storage";
import { useEffect, useState } from "react";

import { usePathname } from "next/navigation";

const withoutMenu = /^\/login|register$/i;

export default function Header() {
  const pathName = usePathname();
  if (withoutMenu.test(pathName)) return null;

  const localTheme = getItem("_theme");
  const [isDark, setIsDark] = useState(localTheme === "dark");

  useEffect(() => {
    // setIsDark(localTheme === "dark");
    const getWindowsTheme = () => {
      return window.matchMedia("(prefers-color-scheme: dark)").matches;
    };
    setIsDark(getWindowsTheme);
  }, []);

  useEffect(() => {
    if (isDark) {
      document.body.classList.add("dark");
    } else {
      document.body.classList.remove("dark");
    }

    setItem("_theme", isDark ? "dark" : "light");
  }, [isDark]);

  return (
    <header className="flex flex-row custom-shadow">
      {/* Sidebar */}
      <SidebarIcon />

      {/* logo */}
      <Logo isDark={isDark} />

      {/* theme */}
      <Theme isDark={isDark} onClick={() => setIsDark(!isDark)} />

      {/* menu */}
      <Menu />
    </header>
  );
}
