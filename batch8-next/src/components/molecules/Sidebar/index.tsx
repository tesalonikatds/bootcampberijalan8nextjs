"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

const menuSideBar = [
  { name: "Now Playing", href: "/now-playing" },
  { name: "Popular", href: "/popular" },
  { name: "Top Rated", href: "/top-rated" },
  { name: "Upcoming", href: "/upcoming" },
];

export default function Sidebar() {
  const pathName = usePathname();
  if (pathName === "/login") return null;
  return (
    <div className="custom-shadow">
      <ul className="text-[18px]">
        {menuSideBar.map((item, index) => (
          <Link key={index} href={item.href}>
            <li
              className={`${
                pathName === item.href ? "bg-selected" : ""
              } menu-sidebar`}
            >
              {item.name}
            </li>
          </Link>
        ))}
      </ul>
    </div>
  );
}
